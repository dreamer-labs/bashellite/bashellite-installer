FROM centos:7 as bashellite-base
ADD ./install-deps.sh /root/bashellite/.init/install-deps.sh
RUN cd /root/bashellite/.init/ && \
    chmod +x install-deps.sh && \
    ./install-deps.sh && \
    yum clean all;

FROM bashellite-base as bashellite
ADD ./Makefile /root/bashellite/.init/Makefile
RUN  cd /root/bashellite/.init/ && \
     make all;
USER bashellite
CMD /bin/bash
